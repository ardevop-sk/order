package sk.ardevop.order;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class OrderController {

  private final ProductsApiClient productsApiClient;

  public OrderController(ProductsApiClient productsApiClient) {
    this.productsApiClient = productsApiClient;
  }

  @GetMapping("/")
  public String index(Model model) {
    model.addAttribute("products", productsApiClient.findProducts().getBody());
    return "index";
  }
}

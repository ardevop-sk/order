package sk.ardevop.order;

import java.io.File;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.testcontainers.containers.BrowserWebDriverContainer;
import org.testcontainers.containers.BrowserWebDriverContainer.VncRecordingMode;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

@Testcontainers
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class OrderApplicationTests {

	@LocalServerPort
	private int port;

	@Container
	private final BrowserWebDriverContainer<?> container = new BrowserWebDriverContainer<>()
			.withCapabilities(new ChromeOptions())
			.withRecordingMode(VncRecordingMode.RECORD_ALL, new File("/tmp"));

	@Test
	void testIndex() throws InterruptedException {
		WebDriver webDriver = container.getWebDriver();
		webDriver.get("http://host.docker.internal:" + port + "/");
		Thread.sleep(2000);
		webDriver.get("http://host.docker.internal:" + port + "/actuator/health");
		Thread.sleep(2000);
	}

}
